/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.iutamiens.m4102c.tpchat_berges_gennevee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 *
 * @author Dixmi
 */
public class IOCommandes {
    private BufferedReader lectureEcran;
    private PrintStream ecritureEcran;
    private BufferedReader lectureReseau;
    private PrintStream ecritureReseau;
    private Socket reseau;
    
    public IOCommandes(Socket distantSocket) {
        lectureEcran = new BufferedReader( new InputStreamReader( System.in ) );
        ecritureEcran = System.out;
        reseau = distantSocket;
        lectureReseau = null;
        ecritureReseau = null;
        try {
            lectureReseau = new BufferedReader( new InputStreamReader( reseau.getInputStream() ) );
        }
        catch (IOException e) {
            ecrireEcran(e.getMessage());
        }
        try {
            ecritureReseau = new PrintStream( reseau.getOutputStream() );
        }
        catch (IOException e) {
            ecrireEcran(e.getMessage());
        }
    }
    
    public void ecrireEcran(String texte) {
        ecritureEcran.println(texte);
    }
    
    public String lireEcran() {
        String res = "";
        try {
            res = lectureEcran.readLine();
        }
        catch (IOException e) {
            ecrireEcran(e.getMessage());
        }
        return res;
    }
    
    public void ecrireReseau(String texte) {
        ecritureReseau.println(texte);
    }
    
    public String lireReseau() {
        String res = "";
        try {
            res = lectureReseau.readLine();
        }
        catch (IOException e) {
            ecrireEcran(e.getMessage());
        }
        return res;
    }
    
    
}
